FROM golang:latest as builder

RUN mkdir /rt

WORKDIR /rt

COPY . .

WORKDIR /rt/cmd
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

FROM xordiv/docker-alpine-cron

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /rt/cmd .

ENTRYPOINT ["./cmd"]

CMD ["./cmd"]