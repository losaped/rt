package api

import (
	"net/http"

	"bitbucket.org/losaped/rt/api/router"
	"bitbucket.org/losaped/rt/types"
)

type Config struct {
	Addr string `envconfig:"addr"`
}

type Server struct {
	Repo    types.Repository
	Updater types.UpdateManager
	Server  http.Server
}

func Init(c *Config, r types.Repository, upd types.UpdateManager) http.Server {
	api := &Server{
		Repo:    r,
		Updater: upd,
	}

	return http.Server{
		Handler: api.initRouter(),
		Addr:    c.Addr,
	}

}

func (api *Server) initRouter() *router.Router {
	r := router.NewRouter()
	r.Use(Logger())

	r.Handle(`^/reload$`, api.UpdateHandler)
	r.Handle(`/code/([\w\._-]+)$`, api.PhoneCode)
	return r
}
