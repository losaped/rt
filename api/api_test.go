package api

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/losaped/rt/mock"
	"bitbucket.org/losaped/rt/types"
	"github.com/jinzhu/gorm"
)

func TestPhoneCode(t *testing.T) {
	req, err := http.NewRequest("GET", "/code/Bermuda", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	c := &Config{
		Addr: ":3000",
	}

	repo := &mock.Repository{
		PhoneCodeByNameFn: func(name string) (string, error) {
			return "", gorm.ErrRecordNotFound
		},
	}

	server := Init(c, repo, types.UpdateManager{})
	server.Handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %d want %d", status, http.StatusNotFound)
	}
}
