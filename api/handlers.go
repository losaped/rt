package api

import (
	"net/http"

	"bitbucket.org/losaped/rt/api/router"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type JSONResponse struct {
	Status int
	Data   interface{} `json:"data,omitempty"`
}

func (api *Server) UpdateHandler(c *router.Context) {
	if err := api.Updater.Update(api.Repo); err != nil {
		c.Error(http.StatusInternalServerError, err, "unable to update repository")
		return
	}

	resp := JSONResponse{
		Status: http.StatusOK,
	}

	c.JSON(http.StatusOK, resp)
}

func (api *Server) PhoneCode(c *router.Context) {
	if len(c.Params) == 0 || len(c.Params[0]) == 0 {
		http.Error(c.ResponseWriter, "wrong country name", http.StatusBadRequest)
	}

	phoneCode, err := api.Repo.PhoneCodeByName(c.Params[0])
	if err != nil {
		c.Logger.Error(err)

		if errors.Cause(err) == gorm.ErrRecordNotFound {
			http.Error(c.ResponseWriter, "not found", http.StatusNotFound)
			return
		}

		http.Error(c.ResponseWriter, "Houston we have a problem", http.StatusInternalServerError)
		return
	}

	resp := JSONResponse{
		Status: http.StatusOK,
		Data:   map[string]interface{}{"phone_code": phoneCode},
	}

	c.JSON(http.StatusOK, resp)
}
