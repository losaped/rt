package api

import "bitbucket.org/losaped/rt/api/router"

// Logger log request info
func Logger() router.Handler {
	return func(c *router.Context) {
		c.Logger.Infof("from %s", c.RemoteAddr)
		c.Next()
	}
}
