package router

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

const (
	MIME_JSON = `application/json`
)

type Context struct {
	*http.Request
	http.ResponseWriter
	Logger          *logrus.Entry
	Params          []string
	handlerChain    []Handler
	handlerPosition int
}

func (c *Context) JSON(code int, body interface{}) {
	if body != nil {
		raw, err := json.Marshal(body)
		if err != nil {
			c.Error(http.StatusInternalServerError, err, "")
			return
		}

		c.ResponseWriter.Header().Set("Content-type", MIME_JSON)
		c.ResponseWriter.Write(raw)
		return
	}
}

func (c *Context) Error(code int, err error, msg string) {
	c.Logger.Error(err)
	http.Error(c.ResponseWriter, msg, code)
}

func (c *Context) Next() {
	if c.handlerPosition >= len(c.handlerChain) {
		return
	}

	c.handlerPosition++
	c.handlerChain[c.handlerPosition](c)
}
