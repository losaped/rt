package router

import (
	"html"
	"net/http"
	"regexp"

	log "github.com/sirupsen/logrus"
)

type Handler func(*Context)

type route struct {
	Pattern *regexp.Regexp
	Handler Handler
}

type Router struct {
	routes       []route
	DefaultRoute Handler
	middlewares  []Handler
}

func NewRouter() *Router {
	router := &Router{
		DefaultRoute: func(c *Context) {
			c.ResponseWriter.WriteHeader(http.StatusNotFound)
		},
	}

	return router
}

func (router *Router) Use(h Handler) {
	router.middlewares = append(router.middlewares, h)
}

func (router *Router) Handle(pattern string, handler Handler) {
	re := regexp.MustCompile(pattern)
	route := route{Pattern: re, Handler: handler}
	router.routes = append(router.routes, route)
}

func (router *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := &Context{
		Request:        r,
		ResponseWriter: w,
		Logger:         getLogger(r),
		handlerChain:   router.middlewares,
	}

	var curHandler Handler
	for _, rt := range router.routes {
		if matches := rt.Pattern.FindStringSubmatch(ctx.URL.Path); len(matches) > 0 {
			if len(matches) > 1 {
				ctx.Params = matches[1:]
			}

			curHandler = rt.Handler
			break
		}
	}

	if curHandler == nil {
		curHandler = router.DefaultRoute
	}

	ctx.handlerChain = append(ctx.handlerChain, curHandler)
	ctx.handlerChain[0](ctx)
}

func getLogger(r *http.Request) *log.Entry {
	return log.WithFields(log.Fields{
		"path":   html.EscapeString(r.URL.Path),
		"method": r.Method,
	})
}
