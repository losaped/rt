package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/losaped/rt/api"
	"bitbucket.org/losaped/rt/json"
	pg "bitbucket.org/losaped/rt/postgres"
	"bitbucket.org/losaped/rt/types"
	"bitbucket.org/losaped/rt/web"
	"github.com/kelseyhightower/envconfig"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

func main() {

	log.Info("start states service")
	updConfig, err := updateConfig()
	if err != nil {
		log.Fatal(err)
	}

	log.Info("updConfig", updConfig)

	apiConfig, err := serverConfig()
	if err != nil {
		log.Fatal(err)
	}

	log.Info("apiConfig", apiConfig)

	dbCfg, err := dbBConfig()
	if err != nil {
		log.Fatal(err)
	}

	log.Info("dbConfig", dbCfg)
	log.Info("trying to connect db")
	repo, err := pg.NewRepository(dbCfg)
	if err != nil {
		log.Fatal(err)
	}

	log.Info("db connection established")

	log.Info("trying to execute migrate")
	if err := repo.Migrate(); err != nil {
		log.Fatal(err)
	}

	updater := types.UpdateManager{
		UpdateConfig: updConfig,
		SourceGetter: web.Client{},
		SourceParser: json.Parser{},
	}

	log.Info("trying to update repository")
	if err := updater.Update(repo); err != nil {
		log.Fatal(err)
	}

	server := api.Init(apiConfig, repo, updater)
	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown: ", err)
	}

	log.Println("Server stopped")
}

// GetUpdateConfig returns initialized config
func updateConfig() (*types.UpdateConfig, error) {
	config := &types.UpdateConfig{}
	return config, errors.Wrap(envconfig.Process("UPDATE", config), "parse update config")
}

// GetServerConfig returns initialized config
func serverConfig() (*api.Config, error) {
	config := &api.Config{}
	return config, errors.Wrap(envconfig.Process("SERVER", config), "parse server config")
}

// GetDBConfig returns initialized config
func dbBConfig() (*pg.Config, error) {
	config := &pg.Config{}
	return config, errors.Wrap(envconfig.Process("DB", config), "parse database config")
}
