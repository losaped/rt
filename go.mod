module bitbucket.org/losaped/rt

go 1.12

require (
	github.com/jinzhu/gorm v1.9.8
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
)
