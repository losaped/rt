package json

import (
	"encoding/json"

	"github.com/pkg/errors"
)

type Parser struct{}

func (p Parser) Parse(raw []byte) (map[string]string, error) {
	m := make(map[string]string)
	err := json.Unmarshal(raw, &m)
	return m, errors.Wrap(err, "on json unmarshal")
}
