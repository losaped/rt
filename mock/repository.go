package mock

import "bitbucket.org/losaped/rt/types"

type Repository struct {
	UpdateFn          func(data []types.Country) error
	PhoneCodeByNameFn func(name string) (string, error)
	MigrateFn         func() error
}

func (r *Repository) Update(data []types.Country) error {
	return r.UpdateFn(data)
}

func (r *Repository) PhoneCodeByName(name string) (string, error) {
	return r.PhoneCodeByNameFn(name)
}

func (r *Repository) Migrate() error {
	return r.MigrateFn()
}
