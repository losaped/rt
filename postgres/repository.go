package postgres

import (
	"fmt"
	"strings"

	"bitbucket.org/losaped/rt/types"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

const (
	queryTmpl            = `INSERT INTO countries (code, name, phone_code) VALUES %s`
	prepareStatementTmpl = `(?, ?, ?)`
	createTableScript    = `CREATE TABLE IF NOT EXISTS countries (
		code varchar(10),
		name varchar(200),
		phone_code varchar(100),
		PRIMARY KEY(code)
   );
   
	CREATE INDEX IF NOT EXISTS uppercase_name on countries(UPPER(name));
   `
)

type Config struct {
	ConnString string `envconfig:"CONN_STRING"`
}

// Repository implements Repository interface with postgress as storage
type Repository struct {
	db *gorm.DB
}

// NewRepository init db connection
func NewRepository(c *Config) (types.Repository, error) {
	fmt.Println("CONSTRING", c.ConnString)
	db, err := gorm.Open("postgres", c.ConnString)
	if err != nil {
		return nil, errors.Wrap(err, "on db connect")
	}

	return &Repository{
		db: db,
	}, nil
}

// Update truncate tables and insert new data
func (repo *Repository) Update(countries []types.Country) error {
	prepares := make([]string, 0, len(countries))
	values := make([]interface{}, 0, len(countries)*3)

	for _, country := range countries {
		prepares = append(prepares, prepareStatementTmpl)
		values = append(values, country.Code, country.Name, country.PhoneCode)
	}

	query := fmt.Sprintf(queryTmpl, strings.Join(prepares, ","))
	tx := repo.db.Begin()

	if err := tx.Exec("truncate table countries").Error; err != nil {
		tx.Rollback()
		return errors.Wrap(err, "on truncate table")
	}

	if err := tx.Exec(query, values...).Error; err != nil {
		tx.Rollback()
		return errors.Wrap(err, "on insert data")
	}

	tx.Commit()
	return nil
}

func (repo *Repository) PhoneCodeByName(name string) (string, error) {
	country := &types.Country{}
	if err := repo.db.First(country, "upper(name) = ?", strings.ToUpper(name)).Error; err != nil {
		return "", errors.Wrap(err, "on select country")
	}
	return country.PhoneCode, nil
}

func (repo *Repository) Migrate() error {
	err := repo.db.Exec(createTableScript).Error
	return errors.Wrap(err, "on migrate")
}
