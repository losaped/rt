package types

// Country represents main business entity
type Country struct {
	Code      string
	Name      string
	PhoneCode string
}
