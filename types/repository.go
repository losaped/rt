package types

// Repository db mediator
type Repository interface {
	Update(data []Country) error
	PhoneCodeByName(name string) (string, error)
	Migrate() error
}
