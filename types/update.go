package types

import (
	"strings"

	"github.com/pkg/errors"
)

type UpdateConfig struct {
	CountriesPath  string `envconfig:"COUNTRIES_PATH"`
	PhoneCodesPath string `envconfig:"PHONE_CODES_PATH"`
}

// SourceGetter allows get source for update by address
type SourceGetter interface {
	GetSource(path string) ([]byte, error)
}

// Parser must parse data
type Parser interface {
	Parse(data []byte) (map[string]string, error)
}

// UpdateManager manages the update process
type UpdateManager struct {
	*UpdateConfig
	SourceGetter SourceGetter
	SourceParser Parser
}

// Update receives data from source, parce them to Country array and update repository
func (upd UpdateManager) Update(repo Repository) error {
	rawNames, err := upd.SourceGetter.GetSource(upd.CountriesPath)
	if err != nil {
		return errors.Wrap(err, "on getting country names")
	}

	rawPhoneCodes, err := upd.SourceGetter.GetSource(upd.PhoneCodesPath)
	if err != nil {
		return errors.Wrap(err, "on getting country phone codes")
	}

	mapNames, err := upd.SourceParser.Parse(rawNames)
	if err != nil {
		return errors.Wrap(err, "on parse country names")
	}

	mapPhoneCodes, err := upd.SourceParser.Parse(rawPhoneCodes)
	if err != nil {
		return errors.Wrap(err, "on parse country phone codes")
	}

	countries := make([]Country, 0, len(mapNames))
	for code, name := range mapNames {
		country := Country{
			Code:      strings.TrimSpace(code),
			Name:      strings.TrimSpace(name),
			PhoneCode: strings.TrimSpace(mapPhoneCodes[code]),
		}

		countries = append(countries, country)
	}

	return errors.Wrap(repo.Update(countries), "on update repository")
}
