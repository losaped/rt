package web

import (
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
)

type Client struct{}

func (c Client) GetSource(path string) ([]byte, error) {
	resp, err := http.Get(path)
	if err != nil {
		return nil, errors.Wrap(err, "on getting raw data from source")
	}

	defer resp.Body.Close()

	raw, err := ioutil.ReadAll(resp.Body)
	return raw, errors.Wrap(err, "on reading raw data")
}
